# frozen_string_literal: true

module LsifParser
  # Processes and stores ranges data and links to their hovers and definitions
  class Ranges
    def initialize
      @ranges = {}
      @def_refs = {}
      @hovers = Hovers.new
    end

    def read(line)
      case line['label']
      when 'range'
        add(line)
      when 'item'
        add_item(line)
      else
        @hovers.read(line)
      end
    end

    def find(id)
      @ranges[id]
    end

    def hover_for(id)
      @hovers.for(id)
    end

    def def_ref_for(id)
      @def_refs[id]
    end

    private

    def add(line)
      id = line['id']
      start_data = line['start']

      @ranges[id] = {
        start_line: start_data['line'],
        start_char: start_data['character']
      }
    end

    def add_item(line)
      type = line['property']

      return unless %w[definitions references].include?(type)

      line['inVs'].each do |range_id|
        @ranges[range_id]&.merge!(ref_id: line['outV'])
      end

      return unless type == 'definitions'

      @def_refs[line['outV']] = {
        id: line['inVs'].first,
        doc_id: line['document']
      }
    end
  end
end
