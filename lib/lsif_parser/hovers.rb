# frozen_string_literal: true

module LsifParser
  # Processes and stores hovers data for ranges
  class Hovers
    def initialize
      @data = {}
      @refs = {}
      @hovers = {}
    end

    def read(line)
      case line['label']
      when 'hoverResult'
        add_data(line)
      when 'textDocument/hover'
        add(line)
      when 'textDocument/references'
        add_refs(line)
      end
    end

    def for(id)
      ref_id = @refs[id]
      hover_id = @hovers[ref_id]
      @data[hover_id]
    end

    private

    def add(line)
      @hovers[line['outV']] = line['inV']
    end

    def add_data(line)
      @data[line['id']] = line['result']['contents']
    end

    def add_refs(line)
      @refs[line['inV']] = line['outV']
    end
  end
end
