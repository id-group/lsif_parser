# frozen_string_literal: true

RSpec.describe LsifParser::Ranges do
  let(:first_range) do
    { 'id' => 1, 'label' => 'range', 'start' => { 'line' => 1, 'character' => 2 }}
  end
  let(:second_range) do
    { 'id' => 2, 'label' => 'range', 'start' => { 'line' => 5, 'character' => 4 }}
  end
  let(:ref_id) { 3 }
  let(:def_item) do
    { 'id' => 4, 'label' => 'item', 'property' => 'definitions', 'outV' => ref_id, 'inVs' => [1], 'document' => 6 }
  end
  let(:ref_item) do
    { 'id' => 5, 'label' => 'item', 'property' => 'references', 'outV' => ref_id, 'inVs' => [2] }
  end

  it 'returns hover content for a range id' do
    subject.read(first_range)
    subject.read(second_range)
    subject.read(def_item)
    subject.read(ref_item)

    expect(subject.find(1)).to eq({
      start_line: 1,
      start_char: 2,
      ref_id: ref_id
    })
    expect(subject.find(2)).to eq({
      start_line: 5,
      start_char: 4,
      ref_id: ref_id
    })
    expect(subject.def_ref_for(ref_id)).to eq({
      id: 1,
      doc_id: 6
    })
  end
end
