# frozen_string_literal: true

RSpec.describe LsifParser::Docs do
  subject { described_class.new(prefix) }

  context 'adding contains line' do
    let(:prefix) { '' }
    let(:contains_item) do
      { 'id' => 5, 'label' => 'contains', 'outV' => 1, 'inVs' => [2, 3] }
    end

    it 'stores values in the doc ranges' do
      subject.read(contains_item)

      expect(subject.doc_ranges).to eq(1 => [2, 3])
    end
  end

  context 'adding document lines' do
    let(:prefix) { '/Users/nested' }

    let(:first_doc) do
      { 'id' => 1, 'label' => 'document', 'uri' => 'file:///Users/nested/file.rb' }
    end
    let(:second_doc) do
      { 'id' => 2, 'label' => 'document', 'uri' => 'file:///Users/nested/folder/file.rb' }
    end
    let(:doc_with_wrong_prefix) do
      { 'id' => 3, 'label' => 'document', 'uri' => 'file:///Users/wrong/file.rb' }
    end

    it 'stores docs without prefix' do
      subject.read(first_doc)
      subject.read(second_doc)
      subject.read(doc_with_wrong_prefix)

      expect(subject.find(1)).to eq('file.rb')
      expect(subject.find(2)).to eq('folder/file.rb')
      expect(subject.find(3)).to eq('file:///Users/wrong/file.rb')

      doc_paths = []
      subject.each { |id, path| doc_paths << path }
      expect(doc_paths).to eq(['file.rb', 'folder/file.rb'])
    end
  end

  context 'prefix without prepended slash' do
    let(:prefix) { 'Users/nested' }

    let(:doc) do
      { 'id' => 1, 'label' => 'document', 'uri' => 'file:///Users/nested/file.rb' }
    end

    it 'stores docs without prefix' do
      subject.read(doc)

      expect(subject.find(1)).to eq('file.rb')
    end
  end

  context 'prefix with appended slash' do
    let(:prefix) { 'Users/nested/' }

    let(:doc) do
      { 'id' => 1, 'label' => 'document', 'uri' => 'file:///Users/nested/file.rb' }
    end

    it 'stores docs without prefix' do
      subject.read(doc)

      expect(subject.find(1)).to eq('file.rb')
    end
  end
end
