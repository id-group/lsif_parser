# frozen_string_literal: true

require 'lsif_parser/highlight'

module LsifParser
  # Serializes ranges data with hovers and definition paths
  class RangesStrategy
    def process(ranges, docs)
      ranges&.map do |range_id|
        range = docs.ranges.find(range_id)
        ref_id = range[:ref_id]

        {
          start_line: range[:start_line],
          start_char: range[:start_char],
          definition_path: def_path_for(docs, ref_id),
          hover: hover_for(docs, ref_id)
        }
      end
    end

    private

    def hover_for(docs, ref_id)
      docs.ranges.hover_for(ref_id)&.map do |hover|
        # Documentation for a method which is added as comments
        # is stored as a raw string value in LSIF file
        next { value: hover } unless hover.is_a?(Hash)

        { language: hover['language'], value: Highlight.run(hover) }
      end
    end

    def def_path_for(docs, ref_id)
      def_ref = docs.ranges.def_ref_for(ref_id)
      range = def_ref && docs.ranges.find(def_ref[:id])

      return unless range

      "#{docs.find(def_ref[:doc_id])}#L#{range[:start_line] + 1}"
    end
  end
end
